# Repositorio "Ejercicio Angular termino semana 2"

**Objetivo**
- Configurar herramienta postman para hacer un llamado a las API`s publicadas
- Construir un sitio básico que sea capaz de listar el ID de cada uno de los paraderos y su respectivo nombre.
- Entregar los códigos con un readme configurado en Gitlab compatible Zepto.js and iPad;

**Instalacion**

en la consola git-bash ejecutar el siguiente comando para descargar la carpeta de bootstrap 
- npm install ngf-bootstrap --save

**Enlaces**

Para la realizacion de este ejercicio se utilizo la siguiente API abierta de ejemplo de los paraderos de transantiago:
- https://api.scltrans.it/v1/stops

Eduardo Pastene.
