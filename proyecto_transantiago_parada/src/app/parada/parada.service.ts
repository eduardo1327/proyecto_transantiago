import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})

export class ParadaService {
    constructor(private http: HttpClient) {

    }
    
    getData() {
        let url = "https://api.scltrans.it/v1/stops";
        return this.http.get(url);
    }
}
