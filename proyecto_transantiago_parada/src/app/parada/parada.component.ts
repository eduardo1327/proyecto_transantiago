import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-parada',
  templateUrl: './parada.component.html',
  styleUrls: ['./parada.component.css']
})
export class ParadaComponent implements OnInit {

  data: any
  datas= [];
  constructor(private http:HttpClient) { }

  ngOnInit(): void {
    this.http.get('https://api.scltrans.it/v1/stops')
      .subscribe(Response => {
        console.log(Response)
        this.data = Response;
        this.datas = this.datas;
      });
  }

}
