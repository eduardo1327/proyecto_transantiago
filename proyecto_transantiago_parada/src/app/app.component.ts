import { Component } from '@angular/core';
import { ParadaService } from './parada/parada.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Parada Transantiago';
  data:any =[];
  constructor(private tablas:ParadaService){

  }
}
